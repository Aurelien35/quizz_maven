package Main;

import java.util.List;

public class QuestionVF extends Question {
    public Boolean reponse;

    public  QuestionVF(String question, Boolean reponse){
        super (question, reponse.toString());
        this.reponse = reponse;
    }
    public String getText() {
        return this.question +System.lineSeparator()+ "Vrai ou Faux?";
    }

    private Boolean correctAnswer(String entry){
        //entry = new entry();


        if((entry.toLowerCase().equals("oui")) || (entry.toLowerCase().equals("vrai")) || (entry.toLowerCase().equals("true"))) {
                return true;
        }
        else if((entry.toLowerCase().equals("faux")) || (entry.toLowerCase().equals("non")) || (entry.toLowerCase().equals("false"))){
                return false;
        } else{
            return null;
        }
    }
    public Boolean tryAnswer (String entry){
        // on compare la réponse de l'utilisateur avec celle de la question
        if(correctAnswer(entry) == (this.reponse)){
            return true;
        }
        else{
            return false;
        }

    }
}