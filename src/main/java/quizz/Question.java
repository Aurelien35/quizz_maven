package Main;

import org.apache.commons.text.similarity.LevenshteinDistance;

import java.util.ArrayList;

public class Question {
    public String question;
    public String reponse;

    public Question(String question, String reponse) {
        this.question = question;
        this.reponse = reponse;


    }

    public String getText() {
        return this.question;
    }


    public Boolean tryAnswer(String answer){

        LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
        levenshteinDistance.apply(answer, this.reponse);
        if (levenshteinDistance.apply(answer, this.reponse) <2){
        return true;}
        else{ return false;}
    }


}